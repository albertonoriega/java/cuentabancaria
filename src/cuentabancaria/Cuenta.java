
package cuentabancaria;


public class Cuenta {
    // PROPIEDADES
    String titular;
    double cantidad;
    
    //CONSTRUCTOR QUE TE PIDE LAS DOS PROPIEDADES
    public Cuenta(String titular, double cantidad) {
        this.titular = titular;
        this.cantidad = cantidad;
    }
    
    //CONSTRUCTOR QUE TE PIDE SOLO EL TITULAR
    public Cuenta(String titular) {
        this.titular = titular;
    }

    // GETTERS Y SETTERS
    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }
    
    // METODO INGRESAR
    public void ingresar (double cantidad){
        if (cantidad>0){
            this.cantidad+=cantidad;
        }
    }
    
    // METODO RETIRAR
    public void retirar (double cantidad){
        this.cantidad-=cantidad;
        if (this.cantidad<0){
            this.cantidad=0;
        }
    }
    
    // METODO INFO
    
    public void info() {
        System.out.println("El titular "+ this.titular + " tiene " + this.cantidad + " euros en la cuenta"); 
    }
    
    
}
