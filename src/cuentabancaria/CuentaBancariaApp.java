
package cuentabancaria;


public class CuentaBancariaApp {

    public static void main(String[] args) {
        
        // Inicializamos objeto de clase cuenta
        Cuenta cliente1; 
        // Introducimos titular y cantidad por medio del contructor
        cliente1= new Cuenta("Eva", 2000);  
        // Imprimimos la cantidad
        System.out.println(cliente1.cantidad);
        // Ingresamos 1000 con el metodo ingresar
        cliente1.ingresar(1000);
        
        System.out.println(cliente1.cantidad);
        // Ingresamos -100 con el metodo ingresar 
        cliente1.ingresar(-100);
        // comprobamos que al introducir cantidad negativa no varía la cantidad
        System.out.println(cliente1.cantidad);
        // Retiramos 2000 con el metodo retirar
        cliente1.retirar(2000);
        
        System.out.println(cliente1.cantidad);
        // Retiramos 2000 con el metodo retirar
        cliente1.retirar(2000);
        // comprobamos que la cantidad pese a ser negativa, pasa a valer 0
        System.out.println(cliente1.cantidad);
        // Imprimimos el metodo info que nos da la informacion de la cuenta
        // no ponemos System.ou.print porque el metodo es quein te lo imprime por defecto
        cliente1.info();
        
        // Inicializamos otro objeto de clase cuenta
        Cuenta cliente2;
        // Introducimos solo el titular por medio del contructor
        cliente2 = new Cuenta("Pablo");
        // Ingresamos 2000 con el metodo ingresar
        cliente2.ingresar(2000);
        // Imprimimos la informacion de la cuenta usando el metodo info
        cliente2.info();
       
        
        CuentaDivisa cliente3;
        cliente3= new CuentaDivisa("Dolares", 1.02, "Paco", 1000);
        cliente3.enEuros();
        cliente3.info();
        
        CuentaDivisa cliente4;
        cliente4= new CuentaDivisa("Libras", 1.15, "Maria", 1000);
        cliente4.enEuros();
        cliente4.info();
    }
               
}
