
package cuentabancaria;

public class CuentaDivisa extends Cuenta{
    String divisa;
    double cambio;

    public CuentaDivisa(String divisa, double cambio, String titular, double cantidad) {
        super(titular, cantidad);
        this.divisa = divisa;
        this.cambio = cambio;
                      
    }
    
    public void enEuros(){
        
         this.cantidad=this.cantidad*this.cambio;
        
        }

    @Override
    public void info() {
        super.info(); 
    }      
    }
